public class Merchandise {
	
	String name; 
	String unit; 
	double netPrice; 
	double grossPrice; 
	double totalNetPrice; 
	double totalGrossPrice; 
	int amount; 
	int tax; 
	
////////////////////////////////////////////////////////	
	String getName() {
        return name;
    }
	
	void setName(String name) {
		this.name = name;
	}	
////////////////////////////////////////////////////////
	String getUnit() {
        return unit;
    }
	
	void setUnit(String unit) {
		this.unit = unit;
	}
////////////////////////////////////////////////////////
	double getNetPrice() {
        return netPrice;
    }
	
	void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}
////////////////////////////////////////////////////////	
	double getGrossPrice() {
        return grossPrice;
    }
	
	void setGrossPrice() {
		this.grossPrice = (getNetPrice() * (getTax() + 100)/100);
	}
////////////////////////////////////////////////////////		
	double getTotalNetPrice() {
        return totalNetPrice;
    }
	
	void setTotalNetPrice() {
		this.totalNetPrice = getNetPrice() * getAmount();
	}
////////////////////////////////////////////////////////	
	double getTotalGrossPrice() {
        return totalGrossPrice;
    }
	
	void setTotalGrossPrice() {
		this.totalGrossPrice = getGrossPrice() * getAmount();
	}
////////////////////////////////////////////////////////	
	int getAmount() {
        return amount;
    }
	
	void setAmount(int amount) {
		this.amount = amount;
	}
////////////////////////////////////////////////////////	
	int getTax() {
        return tax;
    }
	
	void setTax(int tax) {
		this.tax = tax;
	}
///////////////////////////////////////////////////////	
	
	
	Merchandise(String name, String unit, double netPrice, int amount, int tax) {
		setName(name);
		setUnit(unit);
		setNetPrice(netPrice);
		setAmount(amount);
		setTax(tax);
		setGrossPrice();
		setTotalNetPrice();
		setTotalGrossPrice();
	}
	
	
	@Override
    public String toString() {
        return "Produkt: " + getName() + " , Ilosc: " + getAmount() + " " + getUnit() + ", Cena netto jedn.: " + getNetPrice() + " zl, Cena brutto jedn.: " 
    + getGrossPrice() + " zl, Wartosc calkowita netto: " + getTotalNetPrice() + " zl , Wartosc calkowita brutto: " + getTotalGrossPrice() + " zl, podatek: " + getTax() + "%";
    }
	
}