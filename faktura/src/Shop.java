import java.util.ArrayList;




public class Shop {

	public static void main(String[] args) {
		
		ArrayList<Invoice> invoices = new ArrayList<>();
		
		Invoice invoice1 = new Invoice();
		Invoice invoice2 = new Invoice();
		
		invoice1.towar.add(new Merchandise("Czekolada Milka", "sztuk", 5, 20, 18));
		invoice1.towar.add(new Merchandise("Mleko 2%", "sztuk", 3, 30, 15));
		invoice1.towar.add(new Merchandise("Sok Tymbark", "sztuk", 2, 23, 18));
		
		invoice1.calculate_and_print();
	
		invoice2.towar.add(new Merchandise("Zeszyt w kratke", "sztuk", 5, 20, 15));
		invoice2.towar.add(new Merchandise("Dlugopisy", "sztuk", 1.6, 100, 13));
		invoice2.towar.add(new Merchandise("Pomarancze", "kg", 10, 28, 18));
		
		invoice2.calculate_and_print();

		
	}

}